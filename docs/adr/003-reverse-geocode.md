# ADR 3. Reverse Geocode

Date: 2024-05-13

## Status: **accepted**

## Context

Some clock statuses are determined from place information instead of saved
location boundaries per user. For example, determining whether a user is abroad
or at an airport.

Turning lat/lon into place information requires reverse geocoding. There are
many existing API solutions for reverse geocoding as well as some public
options and open datasets. It is possible to host a version of OpenStreetMap's
[Nominatim](https://nominatim.org) but that seems complex and probably costly.

Calling geocoding APIs has the potential to be the most expensive part of this
app and definitely the biggest potential for a surprise bill.

In exploring options, the most important dimenstions to consider are:
* frequency - how often will the api get called
* precision - how granular do we need results to be?
* caching - how high could the cache hit rate go, effected by precision

The free APIs have a very low req/sec limit and sometimes daily limits as well
but if expected usage is below that we could get by or try to combine multiple
free APIs together.

The paid services are usually billed per 1000 requests but frequency can really
drive up cost when using AWS or Google.

Frequency will be determined by the Owntracks monitoring mode. Options include
"move" mode as well as "significant changes only". There are also various
owntracks settings which change how each of these modes works.

The biggest X factor after frequency is precision because that greatly affects
how effective caching is. For example, if we just needed to check which country
a lat/lon is in, we could truncate lat/lons to 1 decimal place and be accurate
enough to determine the ABROAD clock status. Caching in that scenario would let
us stay well below the rate limit of a free API. On the other hand, in order to
support labels like BAR or RESTAURANT, we may need 5 decimal places in order to
get < 2 meters of precision necessary to determine which building someone is
in. Caching lat/lons with 5 decimals is possible but the hit rate would suffer
even for the same person staying largely in the same place.

Currently the only reverse-geocoded clock status' are ABROAD, AIRPORT, and
HOSPITAL so we could probably get away with 4 decimal places (< 12 meters
precision). Adding other status options for places requiring more precision
could impact this decision.

There's also a question of whether caching would be effective when reverse
geocoding for this specific use case: https://opencagedata.com/api#caching

I have no idea how to estimate the effectiveness of caching before launch so
for now I'm going to be conservative and assume no caching when estimating
costs.

Monthly load estimate for # users and average update interval per user:
| # users | 1 per min | 1 per 5 min | 1 per 15 min | 1 per 30 min |
|---------|-----------|-------------|--------------|--------------|
|       1 |    43,200 |       8,640 |        2,880 |        1,440 |
|       5 |   216,000 |      43,200 |       14,400 |        7,200 |
|      10 |   432,000 |      86,400 |       28,800 |       14,400 |
|      25 | 1,080,000 |     216,000 |       72,000 |       36,000 |
|      50 | 2,160,000 |     432,000 |      144,000 |       72,000 |

Monthly [OpenCage](https://opencagedata.com/pricing) cost without caching:
| # users | 1 per min | 1 per 5 min | 1 per 15 min | 1 per 30 min |
|---------|-----------|-------------|--------------|--------------|
|       1 |      free |        free |         free |         free |
|       5 |       $50 |        free |         free |         free |
|      10 |      $125 |         $50 |         free |         free |
|      25 |      $500 |         $50 |         free |         free |
|      50 |      $500 |        $125 |          $50 |         free |

Monthly [AWS Location](https://aws.amazon.com/location/pricing) cost without caching:
| # users | 1 per min | 1 per 5 min | 1 per 15 min | 1 per 30 min |
|---------|-----------|-------------|--------------|--------------|
|       1 |       $22 |          $5 |           $2 |           $1 |
|       5 |      $108 |         $22 |           $8 |           $4 |
|      10 |      $220 |         $50 |          $15 |           $8 |
|      25 |      $540 |        $108 |          $36 |          $18 |
|      50 |     $1080 |        $220 |          $72 |          $36 |

## Decision

Use [OpenCage](https://opencagedata.com) for reverse-geocoding.

Try to keep the average update frequency per user to once per 15 min over
the month using Owntracks settings.

Punt cache implementation for now since it's ambiguous how helpful it would be
for this use case.

## Consequences

Detecting current location country becomes possible as well as detecting
whether a user is at an airport or a hospital.

Geocoding during development and for a small user base should be free as long
as expectations about per user update frequency is close to ~once per 15 min.

Changing geocoding service is easy since there's no cache data model to
migrate.
