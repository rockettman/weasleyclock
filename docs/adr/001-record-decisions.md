# ADR 1. Record decisions

Date: 2024-05-08

## Status: **accepted**

## Context

I'm going to forget certain aspects of how this project was built, especially
years later.

I want to build something that will survive for decades as a fun tool for my
family and community to stay connected virtually even as we live far apart so I
need to make sure I can jump back in to this project and fix bugs or add
features easily even after (multi-)year breaks.

External project documentation can be tedious to maintain and therefore gets
outdated quickly or skipped altogether.

I'd like to record the motivation behind certain decisions in a light-weight way
which exists alongside the code.

## Decision

Use [(Architecture) Decision Records](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions),
as described by Michael Nygard.

## Consequences

See Michael Nygard's article, linked above.
