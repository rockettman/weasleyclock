# ADR 2. Data Model

Date: 2024-05-08

## Status: **accepted**

## Context

Need to represent the various users and keep track of their latest location.

Also need to represent clocks as well as which users can see which clocks and
have their location shown on those clocks.

Clocks don't show the raw locations of their members, instead raw user
locations are interpreted and given a label which can then be displayed on the
clocks they belong to.

Users should be able to label locations by type so that their raw location can
be interpreted as a label like "HOME" or "WORK".

Some other locations my not require a user label like "AIRPORT" or "ABROAD".

Clocks don't all have to show the same location labels, one friend group or
family may have different custom locations or supported labels that they care
about and want displayed on their clock.

## Decision

![Data model](../images/erd-v1.webp "Weasley Clock Data Model")

Will use the electrodb library to implement the above data model in a single
DynamoDB table.

![Dynamo single-table design](../images/dynamodb-v1.webp "DynamoDB Single-Table Design")

## Consequences

Supporting custom location types in a clock will be possible to support in v2
with this data model without being necessary for v1.

Saving user location labels in the database, instead of using Owntracks
waypoints, will be possible to support in v2 without being necessary for v1.

There is a clear definition of ownership and admin rights for each clock.

Single-table architecture is efficient but rigid to change.

Using electrodb makes working with dynamo single-table architectures easier,
especially doing migrations by taking advantage of entity versions.

Using dynamo in on-demand billing mode will make this application very cost
effective especially since I don't know when it will launch and even having the
lowest Aurora serverless v2 configured would cost more while this app gets
developed.

Dynamo will not be able to do geospatial calculations internally so calculating
if user locations are inside saved boundaries would have to be done on the app
side or in another database.

PostGIS may be a better fit for this appliation in the long run but I don't
want to figure out a setup I like for working with Aurora Serverless v2 at the
moment and don't want to overpay for db compute before v1 of this app is
launched.
