# ADR 4. Ops Dashboards

Date: 2024-05-18

## Status: **accepted**

## Context

Having nice dashboards makes it easier to investigate issues and keep track of
key metrics related to scaling, adoption, and cost.

Going over the OpenCage free-tier usage quota could disable the API key so
keeping a pulse on OpenCage usage over time is necessary.

Certain Owntracks configs can have a large impact on usage across AWS and the
OpenCage API. Maintaining visibility into usage will make it safer to change
Owntracks configs.

Centralizing alarm and dashboard creation can make it difficult make changes to
ops infrastructure when the underlying infrastructure changes.

See [Building dashboards for operational visibility](https://aws.amazon.com/builders-library/building-dashboards-for-operational-visibility/)
for a more in depth look at organizing a complete ops dashboard footprint.

## Decision

Implement ops dashboards in a hub and spoke model with a primary dashboard that
links to component specific dashboards. The central dashboard should highlight
key metrics and alarms and link to all the component dashboards.

Strive to maintain locality of behavior by defining component dashboards and
alarms alongside the component infrastructure.

## Consequences

Refactoring ops infrastructure should be easier to reason about since
dashboards can have a narrower focus and alarms exist in the same files as the
infrastructure and processes they track.

Getting a high-level update on the status of WeasleyClock will be straight
forward without dilluting the information available for issue resolution.

Growing the ops footprint organically over time will be easy since new
components get a new dashboard instead of refactoring a giant existing
dashboard.

It could be more difficult to view metrics across components that are related
to the same issue since they will be on separate dashboards.
