import { RuleConfigSeverity, type UserConfig } from "@commitlint/types";

const Config: UserConfig = {
  extends: ["@commitlint/config-conventional"],
  rules: {
    "scope-enum": [RuleConfigSeverity.Error, "always", ["inf", "core", "func", "frontend", "scripts"]],
  },
};

export default Config;
