/// <reference path="./.sst/platform/config.d.ts" />
export default $config({
  app(input) {
    return {
      name: "weasleyclock",
      removal: input.stage === "prod" ? "retain" : "remove",
      home: "aws",
      providers: {
        aws: {
          region: "us-east-2",
          profile: input.stage === "prod" ? "prod" : "dev",
          defaultTags: {
            tags: {
              App: "WeasleyClock",
              Env: input.stage,
            },
          },
        },
        awsx: true,
      },
    };
  },
  async run() {
    const infra = await import("./infra");
    const resources = new aws.resourcegroups.Group("ResourceGroup", {
      name: `${$app.name}_${$app.stage}`,
      description: `Weasley Clock resources in the ${$app.stage} stage`,
      resourceQuery: {
        type: "TAG_FILTERS_1_0",
        query: `{"ResourceTypeFilters":["AWS::AllSupported"],"TagFilters":[{"Key":"App","Values":["WeasleyClock"]},{"Key":"Env","Values":["${$app.stage}"]}]}`,
      },
    })
    return {
      resourceGroup: resources.name,
      dashboard: infra.dashboard.url,
      db: infra.database.name,
      iot_endpoint: infra.iotcore.endpoint,
      auth_appclient: infra.Auth.appClient.id,
      auth_appsecret: infra.Auth.appClient.nodes.client.clientSecret,
      auth_issuer: infra.Auth.issuer,
    };
  },
});
