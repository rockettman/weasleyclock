import { Clock, User } from "@weasleyclock/core/model";
import { LocationType } from "@weasleyclock/core/types";

const USER_ID = "temp_test_user"

const main = async () => {

    console.log(`Creating user [${USER_ID}]...`)
    const user = await User.create(USER_ID)
    console.log(`User: ${JSON.stringify(user, null, 2)}`)

    console.log(`Saving HOME location for [${USER_ID}]...`)
    await User.saveLocation({
        userId: USER_ID,
        name: "Example home location",
        type: LocationType.Home,
        address: "1986 Wembley Way",
    })

    console.log(`Saving WORK location for [${USER_ID}]...`)
    await User.saveLocation({
        userId: USER_ID,
        name: "Example work location",
        type: LocationType.Work,
        address: "Copenhagen, DN",
    })

    console.log(`Saving HAPPY_PLACE location for [${USER_ID}]...`)
    await User.saveLocation({
        userId: USER_ID,
        name: "Example happy place location",
        type: LocationType.HappyPlace,
        address: "Climbing gym",
    })

    console.log(`Listing saved locations for [${USER_ID}]...`)
    const locs = await User.listSavedLocations({ userId: USER_ID })
    console.log(`Saved locs: ${JSON.stringify(locs, null, 2)}`)

    console.log(`Deleting WORK location for [${USER_ID}]...`)
    await User.removeSavedLocation({
        userId: USER_ID,
        locId: locs.data.find(l => l.type === LocationType.Work)?.locId ?? "",
    })

    console.log(`Listing saved locations for [${USER_ID}]...`)
    const locs2 = await User.listSavedLocations({ userId: USER_ID })
    console.log(`Saved locs: ${JSON.stringify(locs2, null, 2)}`)

    console.log(`Creating clock owned by [${USER_ID}]...`)
    const { clock, owner } = await Clock.create({
        name: "Example clock",
        ownerId: USER_ID,
    })
    console.log(`Clock: ${JSON.stringify(clock, null, 2)}`)
    console.log(`Clock Owner: ${JSON.stringify(owner, null, 2)}`)

    console.log(`Listing clocks for [${USER_ID}]...`)
    const clocks = await User.listClocks({ userId: USER_ID })
    console.log(`User's clocks: ${JSON.stringify(clocks, null, 2)}`)

    console.log(`Try to delete [${USER_ID}]...`)
    try {
        await User.remove(USER_ID)
    } catch (err) {
        console.log(err)
    }

    console.log(`Delete clock owned by [${USER_ID}]...`)
    await Clock.remove(clock.clockId)

    console.log(`Delete user [${USER_ID}]...`)
    await User.remove(USER_ID)
};

main().catch(err => {
    console.error(err);
    process.exit(1);
});
