import { Clock, User } from "@weasleyclock/core/model";
import { Role } from "@weasleyclock/core/types";

const USER_ID_1 = "seed_user_1"
const USER_ID_2 = "seed_user_2"

const main = async () => {
    await User.create(USER_ID_1)
    await User.create(USER_ID_2)

    const clock = await Clock.create({
        ownerId: USER_ID_1,
        name: "Clock owned by seed_user_1",
    })
    await Clock.addMember({
        clockId: clock.clock.clockId,
        userId: USER_ID_2,
        role: Role.Member,
    })
};

main().catch(err => {
    console.error(err);
    process.exit(1);
});
