import { User, UserItem } from "@weasleyclock/core/model";

async function listUsers() {
    let users: UserItem[] = []
    let cursor: string | null = null
    do {
        const page = await User.list(cursor ?? undefined)
        users.push(...page.data)
        cursor = page.cursor
    } while (cursor)
    return users
}

const main = async () => {
    const users = await listUsers()
    const tableObjects = users.reduce((acc, val) => {
        return {
            ...acc,
            [val.userId]: {
                home: val.homeCountry,
                "last updated": val.lastLocation?.timestamp,
                label: val.lastLocation?.label,
                country: val.lastLocation?.country,
                address: val.lastLocation?.address,
                "airport?": val.lastLocation?.isAirport,
                "hospital?": val.lastLocation?.isHospital,
                "lon,lat": `${val.lastLocation?.longitude},${val.lastLocation?.latitude}`,
                vel: val.lastLocation?.velocity,
                elev: val.lastLocation?.elevation,
            }
        }
    }, {})
    console.table(tableObjects)
};

main().catch(err => {
    console.error(err);
    process.exit(1);
});
