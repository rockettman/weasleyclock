import { Clock, ClockItem, ClockMemberItem } from "@weasleyclock/core/model"

async function getMembers(clock: ClockItem) {
    let members: ClockMemberItem[] = []
    let cursor: string | null = null
    do {
        const page = await Clock.listMembers({
            clockId: clock.clockId,
            cursor: cursor ?? undefined,
        })
        members.push(...page.data)
        cursor = page.cursor
    } while (cursor)
    return members
}

const main = async () => {
    const clockId = process.argv[2]
    const clock = await Clock.get(clockId)
    if (!clock) {
        console.log(`Clock [${clockId}] does not exist`)
        process.exit(1)
    }

    console.log(`id: ${clockId}`)
    console.log(`name: ${clock.name}`)
    console.log(`loc types: ${JSON.stringify(clock.locationTypes)}`)

    console.log("members:")
    const members = await getMembers(clock)
    const tableObjects = members.reduce((acc, val) => {
        return {
            ...acc,
            [val.userId]: {
                role: val.role,
                status: val.status,
            }
        }
    }, {})
    console.table(tableObjects)
};

main().catch(err => {
    console.error(err);
    process.exit(1);
});
