import { Clock, ClockItem } from "@weasleyclock/core/model"

async function listClocks() {
    let clocks: ClockItem[] = []
    let cursor: string | null = null
    do {
        const page = await Clock.list(cursor ?? undefined)
        clocks.push(...page.data)
        cursor = page.cursor
    } while (cursor)
    return clocks
}

const main = async () => {
    const clocks = await listClocks()
    const tableObjects = clocks.reduce((acc, val) => {
        return {
            ...acc,
            [val.clockId]: {
                name: val.name,
            }
        }
    }, {})
    console.table(tableObjects)
};

main().catch(err => {
    console.error(err);
    process.exit(1);
});
