import { Metrics } from "@aws-lambda-powertools/metrics"
import { Resource } from "sst"

export { MetricUnit } from "@aws-lambda-powertools/metrics"

export function MetricsLogger(serviceName: string) {
    return new Metrics({
        namespace: "WeasleyClock",
        serviceName,
        defaultDimensions: {
            stage: Resource.App.stage,
        },
    })
}
