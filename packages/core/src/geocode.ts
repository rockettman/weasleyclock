import * as opencage from "opencage-api-client"
import { Resource } from "sst"
import { z } from "zod"
import { MetricUnit, MetricsLogger } from "./metrics"


const metrics = MetricsLogger("OpenCage")


const ReverseGeocodeSchema = z.object({
    status: z.object({
        code: z.number().int().positive(),
        message: z.string(),
    }),
    rate: z.object({
        limit: z.number().int(),
        remaining: z.number().int(),
        reset: z.number().int(),
    }),
    results: z.object({
        components: z.object({
            "ISO_3166-1_alpha-3": z.string().length(3),
            _category: z.string(),
            _type: z.string(),
        }),
        formatted: z.string(),
    }).array(),
    timestamp: z.object({
        created_http: z.string(),
        created_unix: z.number().int(),
    }),
})
export type ReverseGeocodeResult = z.infer<typeof ReverseGeocodeSchema>


export async function reverseGeocode(props: {
    lat: number,
    lon: number,
}) {
    const { lat, lon } = props
    // TODO: track potential cache hit rates by precision
    const latlon = `${lat.toFixed(4)}, ${lon.toFixed(4)}`
    const result = await _queryOpenCage({
        query: latlon,
        attempt: 1,
        retries: 3,
        backoff: 1000,
    })
    metrics.publishStoredMetrics()
    return result
}


async function _queryOpenCage(props: {
    query: string,
    attempt: number,
    retries: number,
    backoff: number,
}) {
    if (props.attempt > 1) {
        const maxWaitMillis = props.backoff * props.attempt
        const waitMillis = Math.ceil(Math.random() * maxWaitMillis)
        await new Promise(resolve => setTimeout(resolve, waitMillis))
    }
    const start = Date.now()
    const resp = await opencage.geocode({
        q: props.query,
        key: Resource.OpenCageApiKey.value,
        language: "en",
        no_annotations: 1,
        no_record: 1,
    })
    const msLatency = Date.now() - start
    const result = ReverseGeocodeSchema.parse(resp)
    metrics.addMetric("Requests", MetricUnit.Count, 1)
    metrics.addMetric("Latency", MetricUnit.Milliseconds, msLatency)
    metrics.addMetric(result.status.code.toString(), MetricUnit.Count, 1)
    metrics.addMetric("RemainingQuota", MetricUnit.Count, result.rate.remaining)
    console.log(`Response Status: ${JSON.stringify(result.status)}`)
    if (
        props.attempt <= props.retries &&
        (
            result.status.code == 408    // Timeout
            || result.status.code == 429 // Throttled
        )
    ) {
        return _queryOpenCage({
            ...props,
            attempt: props.attempt + 1,
        })
    }
    return result
}
