import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { EntityConfiguration } from "electrodb";
import { Resource } from "sst";

const client = new DynamoDBClient({});

export const DynamoConfig: EntityConfiguration = {
    table: Resource.Database.name,
    client,
};
