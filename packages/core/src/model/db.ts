import { CollectionResponse, Service } from "electrodb";
import { DynamoConfig } from "./dynamo";
import { User } from "./entities/user";
import { Clock } from "./entities/clock";
import { ClockMember } from "./entities/clock-member";
import { UserLocation } from "./entities/user-location";
import { ClockLocation } from "./entities/clock-location";

export const Database = new Service(
    {
        user: User,
        clock: Clock,
        clockMember: ClockMember,
        userLocation: UserLocation,
        clockLocation: ClockLocation,
    },
    DynamoConfig
);

export type UserCollectionResponse = CollectionResponse<typeof Database, "user">;
export type ClockCollectionResponse = CollectionResponse<typeof Database, "clock">;
