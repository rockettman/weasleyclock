import { Database, UserCollectionResponse } from "../db"
import { LocationType, Role } from "../../types"
import { ClockMemberItem } from "../entities/clock-member"
import { LocationUpdate } from "../entities/user"

export module User {

    export async function list(cursor?: string) {
        return await Database.entities
            .user.scan.go({ cursor })
    }

    export async function create(userId: string) {
        const user = await Database.entities
            .user.create({ userId })
            .go()
        return user.data
    }

    export async function get(userId: string) {
        const user = await Database.entities
            .user.get({ userId })
            .go()
        return user.data
    }

    export async function remove(userId: string) {
        // Get all entities related to this user
        let userIds: string[] = []
        let clockMemberships: ClockMemberItem[] = []
        let locIds: string[] = []
        let cursor: string | null = null
        do {
            const collection: UserCollectionResponse = await Database.collections
                .user({ userId })
                .go({ cursor })
            userIds.push(...collection.data.user.map(u => u.userId))
            clockMemberships.push(...collection.data.clockMember)
            locIds.push(...collection.data.userLocation.map(l => l.locId))
            cursor = collection.cursor
        } while (cursor)
        if (userIds.length != 1) {
            throw Error(`Somehow found multiple user entities for [${userId}]`)
        }
        // Ensure user is not the owner of any clocks
        for (var membership of clockMemberships) {
            if (membership.role === Role.Owner) {
                throw Error(`Cannot delete - user [${userId}] owns clock [${membership.clockId}]`)
            }
        }
        // Remove all clock memberships, saved locations, and delete user
        return await Database.transaction.write(({user, clockMember, userLocation}) => [
            ...clockMemberships.map(({clockId}) => {
                return clockMember.remove({ clockId, userId })
                    .commit()
            }),

            ...locIds.map(locId => {
                return userLocation.remove({ locId })
                    .where((loc, {eq}) => eq(loc.userId, userId))
                    .commit()
            }),

            user.remove({ userId })
                .commit(),
        ]).go()
    }

    export async function setHomeCountry(props: {
        userId: string,
        homeCountry: string,
    }) {
        const { userId, homeCountry } = props
        const user = await Database.entities
            .user.patch({ userId })
            .set({ homeCountry })
            .go()
        return user.data
    }

    export async function setLastLocation(props: {
        userId: string,
        locationUpdate: LocationUpdate,
    }) {
        const { userId, locationUpdate } = props
        const user = await Database.entities
            .user.patch({ userId })
            .set({ lastLocation: locationUpdate })
            .go()
        return user.data
    }

    export async function listSavedLocations(props: {
        userId: string,
        cursor?: string,
    }) {
        const { userId, cursor } = props
        return await Database.entities
            .userLocation.query.byUser({ userId })
            .go({ cursor })
    }

    export async function saveLocation(props: {
        userId: string,
        name: string,
        type: LocationType,
        address?: string,
        boundary?: string,
    }) {
        if (!props.address && !props.boundary) {
            throw Error("Must provide either address or boundary geometry")
        }
        const loc = await Database.entities
            .userLocation.create(props)
            .go()
        return loc.data
    }

    export async function getSavedLocation(locId: string) {
        const loc = await Database.entities
            .userLocation.get({ locId })
            .go()
        return loc.data
    }

    export async function updateSavedLocation(props: {
        locId: string,
        name: string,
        type: LocationType,
        address?: string,
        boundary?: string,
    }) {
        const { locId, name, type, address, boundary } = props
        if (!props.address && !props.boundary) {
            throw Error("Must provide either address or boundary geometry")
        }
        const loc = await Database.entities
            .userLocation.patch({ locId })
            .set({ name, type, address, boundary })
            .go()
        return loc.data
    }

    export async function removeSavedLocation(props: {
        userId: string,
        locId: string,
    }) {
        const resp = await Database.entities
            .userLocation.remove({ locId: props.locId })
            .where(({userId}, {eq}) => eq(userId, props.userId))
            .go()
        return resp.data
    }

    export async function listClocks(props: {
        userId: string,
        cursor?: string,
    }) {
        const { userId, cursor } = props
        return await Database.entities
            .clockMember.query.byUser({ userId })
            .go({ cursor })
    }

}
