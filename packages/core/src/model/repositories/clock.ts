import { LocationType, Role } from "../../types"
import { ClockCollectionResponse, Database } from "../db"

export module Clock {

    export async function list(cursor?: string) {
        return await Database.entities
            .clock.scan.go({ cursor })
    }

    export async function create(props: {
        ownerId: string,
        name: string,
    }) {
        const { ownerId, name } = props
        const clock = await Database.entities
            .clock.create({ name })
            .go()
        const owner = await Database.entities
            .clockMember.create({
                clockId: clock.data.clockId,
                userId: ownerId,
                role: Role.Owner,
            })
            .go()
        return {
            clock: clock.data,
            owner: owner.data,
        }
    }

    export async function get(clockId: string) {
        const clock = await Database.entities
            .clock.get({ clockId })
            .go()
        return clock.data
    }

    export async function remove(clockId: string) {
        // Get all entities related to this clock
        let clockIds: string[] = []
        let memberIds: string[] = []
        let locIds: string[] = []
        let cursor: string | null = null
        do {
            const collection: ClockCollectionResponse = await Database.collections
                .clock({ clockId })
                .go({ cursor })
            clockIds.push(...collection.data.clock.map(c => c.clockId))
            memberIds.push(...collection.data.clockMember.map(m => m.userId))
            locIds.push(...collection.data.clockLocation.map(l => l.locId))
            cursor = collection.cursor
        } while (cursor)
        if (clockIds.length != 1) {
            throw Error(`Somehow found multiple clock entities for [${clockId}]`)
        }
        // Remove all clock memberships, custom locations, and delete clock
        return await Database.transaction.write(({clock, clockMember, clockLocation}) => [
            ...memberIds.map(userId => {
                return clockMember.remove({ clockId, userId })
                    .commit()
            }),

            ...locIds.map(locId => {
                return clockLocation.remove({ locId })
                    .where((loc, {eq}) => eq(loc.clockId, clockId))
                    .commit()
            }),

            clock.remove({ clockId })
                .commit()
        ]).go()
    }

    export async function setName(props: {
        clockId: string,
        name: string,
    }) {
        const { clockId, name } = props
        const clock = await Database.entities
            .clock.patch({ clockId })
            .set({ name })
            .go()
        return clock.data
    }

    export async function setLocationTypes(props: {
        clockId: string,
        locationTypes: LocationType[],
    }) {
        const { clockId, locationTypes } = props
        const clock = await Database.entities
            .clock.patch({ clockId })
            .set({ locationTypes })
            .go()
        return clock.data
    }

    export async function listMembers(props: {
        clockId: string,
        cursor?: string,
    }) {
        const { clockId, cursor } = props
        return await Database.entities
            .clockMember.query.byClock({ clockId })
            .go({ cursor })
    }

    export async function addMember(props: {
        clockId: string,
        userId: string,
        role: Role,
    }) {
        const member = await Database.entities
            .clockMember.create(props)
            .go()
        return member.data
    }

    export async function removeMember(props: {
        clockId: string,
        userId: string,
    }) {
        const { clockId, userId } = props
        const resp = await Database.entities
            .clockMember.remove({ clockId, userId })
            .go()
        return resp.data
    }

    export async function setMemberRole(props: {
        clockId: string,
        userId: string,
        role: Role,
    }) {
        const { clockId, userId, role } = props
        const member = await Database.entities
            .clockMember.patch({ clockId, userId })
            .set({ role })
            .go()
        return member.data
    }

    export async function setMemberStatus(props: {
        clockId: string,
        userId: string,
        status: LocationType | string,
    }) {
        const { clockId, userId, status } = props
        const member = await Database.entities
            .clockMember.patch({ clockId, userId })
            .set({ status })
            .go()
        return member.data
    }

    export async function listCustomLocations(props: {
        clockId: string,
        cursor?: string,
    }) {
        const { clockId, cursor } = props
        return await Database.entities
            .clockLocation.query.byClock({ clockId })
            .go({ cursor })
    }

    export async function createCustomLocation(props: {
        clockId: string,
        name: string,
        boundary: string,
    }) {
        const loc = await Database.entities
            .clockLocation.create(props)
            .go()
        return loc.data
    }

    export async function getCustomLocation(locId: string) {
        const loc = await Database.entities
            .clockLocation.get({ locId })
            .go()
        return loc.data
    }

    export async function updateCustomLocation(props: {
        locId: string,
        name: string,
        boundary: string,
    }) {
        const { locId, name, boundary } = props
        const loc = await Database.entities
            .clockLocation.patch({ locId })
            .set({ name, boundary })
            .go()
        return loc.data
    }

    export async function removeCustomLocation(props: {
        clockId: string,
        locId: string,
    }) {
        const resp = await Database.entities
            .clockLocation.remove({ locId: props.locId })
            .where(({clockId}, {eq}) => eq(clockId, props.clockId))
            .go()
        return resp.data
    }

}
