import { Entity, EntityItem } from "electrodb";
import { DynamoConfig } from "../dynamo";
import { cuid2 } from "../cuid2";
import { isCuid } from "@paralleldrive/cuid2";
import { validateClockId } from "./clock";

export const validateClockLocId = (id: string): boolean => {
    return id.startsWith("loc") && isCuid(id.slice(3))
}

export const ClockLocation = new Entity(
    {
        model: {
            entity: "clockloc",
            version: "1",
            service: "weasleyclock",
        },
        attributes: {
            clockId: {
                type: "string",
                required: true,
                readOnly: true,
                validate: (val) => {
                    if (validateClockId(val)) {
                        return ""
                    }
                    return "Not a clockId"
                },
            },
            locId: {
                type: "string",
                required: true,
                readOnly: true,
                default: () => `loc${cuid2()}`,
                set: () => `loc${cuid2()}`,
            },
            createdAt: {
                type: "number",
                required: true,
                readOnly: true,
                default: () => Date.now(),
                set: () => Date.now(),
            },
            updatedAt: {
                type: "number",
                required: true,
                watch: "*",
                default: () => Date.now(),
                set: () => Date.now(),
            },
            name: {
                type: "string",
                required: true,
            },
            boundary: {
                type: "string",
                required: true,
            },
        },
        indexes: {
            _: {
                pk: {
                    field: "pk",
                    composite: ["locId"],
                },
                sk: {
                    field: "sk",
                    composite: [],
                },
            },
            byClock: {
                index: "gsi2",
                collection: "clock",
                type: "clustered",
                pk: {
                    field: "gsi2pk",
                    composite: ["clockId"],
                },
                sk: {
                    field: "gsi2sk",
                    composite: ["createdAt"],
                },
            },
        },
    },
    DynamoConfig
);

export type ClockLocationItem = EntityItem<typeof ClockLocation>;
