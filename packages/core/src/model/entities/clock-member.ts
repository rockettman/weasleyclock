import { Entity, EntityItem } from "electrodb";
import { DynamoConfig } from "../dynamo";
import { Role } from "../../types/role";
import { LocationType } from "../../types/location-type";
import { validateClockLocId } from "./clock-location";
import { validateClockId } from "./clock";

export const ClockMember = new Entity(
    {
        model: {
            entity: "clockmember",
            version: "1",
            service: "weasleyclock",
        },
        attributes: {
            clockId: {
                type: "string",
                required: true,
                readOnly: true,
                validate: (val) => {
                    if (validateClockId(val)) {
                        return ""
                    }
                    return "Not a clockId"
                },
            },
            userId: {
                type: "string",
                required: true,
                readOnly: true,
            },
            createdAt: {
                type: "number",
                required: true,
                readOnly: true,
                default: () => Date.now(),
                set: () => Date.now(),
            },
            updatedAt: {
                type: "number",
                required: true,
                watch: "*",
                default: () => Date.now(),
                set: () => Date.now(),
            },
            status: {
                type: "string",
                required: true,
                default: LocationType.Lost,
                validate: (val) => {
                    const locTypeStringVals = Object.values(LocationType) as string[]
                    if (locTypeStringVals.includes(val)) {
                        return ""
                    }
                    if (validateClockLocId(val)) {
                        return ""
                    }
                    return "ClockMember.status must either be a LocationType or the locId of a ClockLocation"
                },
            },
            role: {
                type: Object.values(Role),
                required: true,
                default: Role.Member,
            },
        },
        indexes: {
            _: {
                pk: {
                    field: "pk",
                    composite: ["clockId", "userId"],
                },
                sk: {
                    field: "sk",
                    composite: [],
                },
            },
            byUser: {
                index: "gsi1",
                collection: "user",
                type: "clustered",
                pk: {
                    field: "gsi1pk",
                    composite: ["userId"],
                },
                sk: {
                    field: "gsi1sk",
                    composite: ["createdAt"],
                },
            },
            byClock: {
                index: "gsi2",
                collection: "clock",
                type: "clustered",
                pk: {
                    field: "gsi2pk",
                    composite: ["clockId"],
                },
                sk: {
                    field: "gsi2sk",
                    composite: ["createdAt"],
                },
            },
        },
    },
    DynamoConfig
);

export type ClockMemberItem = EntityItem<typeof ClockMember>;
