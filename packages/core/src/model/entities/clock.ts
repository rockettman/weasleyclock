import { Entity, EntityItem } from "electrodb";
import { DynamoConfig } from "../dynamo";
import { cuid2 } from "../cuid2";
import { LocationType } from "../../types/location-type";
import { isCuid } from "@paralleldrive/cuid2";

export const validateClockId = (id: string): boolean => {
    return id.startsWith("clock") && isCuid(id.slice(5))
}

export const Clock = new Entity(
    {
        model: {
            entity: "clock",
            version: "1",
            service: "weasleyclock",
        },
        attributes: {
            clockId: {
                type: "string",
                required: true,
                readOnly: true,
                default: () => `clock${cuid2()}`,
                set: () => `clock${cuid2()}`,
            },
            createdAt: {
                type: "number",
                required: true,
                readOnly: true,
                default: () => Date.now(),
                set: () => Date.now(),
            },
            updatedAt: {
                type: "number",
                required: true,
                watch: "*",
                default: () => Date.now(),
                set: () => Date.now(),
            },
            name: {
                type: "string",
                required: true,
            },
            locationTypes: {
                type: "set",
                required: true,
                items: Object.values(LocationType),
                default: [
                    LocationType.Home,
                    LocationType.InTransit,
                    LocationType.Out,
                    LocationType.Abroad,
                    LocationType.Lost,
                ],
            },
        },
        indexes: {
            _: {
                pk: {
                    field: "pk",
                    composite: ["clockId"],
                },
                sk: {
                    field: "sk",
                    composite: [],
                },
            },
            byClock: {
                index: "gsi2",
                collection: "clock",
                type: "clustered",
                pk: {
                    field: "gsi2pk",
                    composite: ["clockId"],
                },
                sk: {
                    field: "gsi2sk",
                    composite: ["createdAt"],
                },
            },
        },
    },
    DynamoConfig
);

export type ClockItem = EntityItem<typeof Clock>;
