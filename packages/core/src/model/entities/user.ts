import { Entity, EntityItem } from "electrodb";
import { DynamoConfig } from "../dynamo";
import { LocationType } from "../../types/location-type";

export const User = new Entity(
    {
        model: {
            entity: "user",
            version: "1",
            service: "weasleyclock",
        },
        attributes: {
            userId: {
                type: "string",
                required: true,
                readOnly: true,
            },
            createdAt: {
                type: "number",
                required: true,
                readOnly: true,
                default: () => Date.now(),
                set: () => Date.now(),
            },
            updatedAt: {
                type: "number",
                required: true,
                watch: "*",
                default: () => Date.now(),
                set: () => Date.now(),
            },
            homeCountry: {
                type: "string",
                required: true,
                default: "USA",
            },
            lastLocation: {
                type: "map",
                required: false,
                properties: {
                    timestamp: { type: "number", required: true },
                    longitude: { type: "number", required: true },
                    latitude: { type: "number", required: true },
                    elevation: { type: "number", required: false },
                    velocity: { type: "number", required: false },
                    address: { type: "string", required: false },
                    country: { type: "string", required: false },
                    label: { type: Object.values(LocationType), required: false },
                    isAirport: { type: "boolean", required: false },
                    isHospital: { type: "boolean", required: false },
                },
            },
        },
        indexes: {
            _: {
                pk: {
                    field: "pk",
                    composite: ["userId"],
                },
                sk: {
                    field: "sk",
                    composite: [],
                },
            },
            byUser: {
                index: "gsi1",
                collection: "user",
                type: "clustered",
                pk: {
                    field: "gsi1pk",
                    composite: ["userId"],
                },
                sk: {
                    field: "gsi1sk",
                    composite: ["createdAt"],
                },
            },
        },
    },
    DynamoConfig
);

export type UserItem = EntityItem<typeof User>;
export type LocationUpdate = NonNullable<UserItem["lastLocation"]>;
