import { Entity, EntityItem } from "electrodb";
import { DynamoConfig } from "../dynamo";
import { cuid2 } from "../cuid2";
import { LocationType } from "../../types/location-type";
import { isCuid } from "@paralleldrive/cuid2";

export const validateUserLocId = (id: string): boolean => {
    return id.startsWith("uloc") && isCuid(id.slice(4))
}

export const UserLocation = new Entity(
    {
        model: {
            entity: "userloc",
            version: "1",
            service: "weasleyclock",
        },
        attributes: {
            userId: {
                type: "string",
                required: true,
                readOnly: true,
            },
            locId: {
                type: "string",
                required: true,
                readOnly: true,
                default: () => `uloc${cuid2()}`,
                set: () => `uloc${cuid2()}`,
            },
            createdAt: {
                type: "number",
                required: true,
                readOnly: true,
                default: () => Date.now(),
                set: () => Date.now(),
            },
            updatedAt: {
                type: "number",
                required: true,
                watch: "*",
                default: () => Date.now(),
                set: () => Date.now(),
            },
            name: {
                type: "string",
                required: true,
            },
            type: {
                type: Object.values(LocationType),
                required: true,
            },
            address: {
                type: "string",
                required: false,
            },
            boundary: {
                type: "string",
                required: false,
            },
        },
        indexes: {
            _: {
                pk: {
                    field: "pk",
                    composite: ["locId"],
                },
                sk: {
                    field: "sk",
                    composite: [],
                },
            },
            byUser: {
                index: "gsi1",
                collection: "user",
                type: "clustered",
                pk: {
                    field: "gsi1pk",
                    composite: ["userId"],
                },
                sk: {
                    field: "gsi1sk",
                    composite: ["createdAt"],
                },
            },
        },
    },
    DynamoConfig
);

export type UserLocationItem = EntityItem<typeof UserLocation>;
