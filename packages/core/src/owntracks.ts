import { Resource } from "sst";
import { OwntracksConfiguration } from "./types";

export function owntracksCfg(userId: string, deviceName: string): OwntracksConfiguration {
    return {
        // Shared options
        _type: "configuration",
        cleanSession: true,
        clientId: deviceName,
        cmd: true,
        deviceId: deviceName,
        host: Resource.IoTCore.endpoint,
        ignoreInaccurateLocations: 0,
        ignoreStaleLocations: 0,
        keepalive: 60 * 31,
        locatorDisplacement: 500,
        locatorInterval: 180,
        mode: 0,
        monitoring: 1,
        mqttProtocolLevel: 4,
        password: Resource.MqttPass.value,
        port: 443,
        pubQos: 0,
        pubRetain: false,
        pubTopicBase: `owntracks/${userId}/${deviceName}`,
        sub: true,
        subQos: 0,
        subTopic: `owntracks/${userId}/+/cmd`,
        tid: "77",
        tls: true,
        username: userId,
        ws: true,

        // Android options
        autostartOnBoot: true,
        locatorPriority: 2,
        notificationLocation: false,
        ping: 30,
        pubExtendedData: true,
        remoteConfiguration: false,

        // iOS options
        allowRemoteLocation: false,
        allowinvalidcerts: false,
        extendedData: true,

        // Unknown
        auth: true,
    }
}
