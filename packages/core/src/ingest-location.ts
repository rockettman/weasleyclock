import { ReverseGeocodeResult, reverseGeocode } from "./geocode";
import { MetricUnit, MetricsLogger } from "./metrics";
import { User } from "./model";
import { OwntracksLocation } from "./types";


const metrics = MetricsLogger("Core")


export async function ingestUserLocation(props: {
    user: string,
    locUpdate: OwntracksLocation,
}) {
    const start = Date.now()
    const place = await reverseGeocode({
        lat: props.locUpdate.lat,
        lon: props.locUpdate.lon,
    })
    await User.setLastLocation({
        userId: props.user,
        locationUpdate: {
            timestamp: props.locUpdate.tst,
            longitude: props.locUpdate.lon,
            latitude: props.locUpdate.lat,
            elevation: props.locUpdate.alt,
            velocity: props.locUpdate.vel,
            label: undefined,
            address: _address(place),
            country: _country(place),
            isAirport: _airport(place),
            isHospital: _hospital(place),
        },
    })
    const msLatency = Date.now() - start
    metrics.addMetric("LocationsProcessed", MetricUnit.Count, 1)
    metrics.addMetric("LocationProcessingLatency", MetricUnit.Milliseconds, msLatency)
    metrics.publishStoredMetrics()
}

function _address(place: ReverseGeocodeResult) {
    return place.status.code == 200
        ? place.results[0].formatted
        : undefined
}

function _country(place: ReverseGeocodeResult) {
    return place.status.code == 200
        ? place.results[0].components["ISO_3166-1_alpha-3"]
        : undefined
}

function _airport(place: ReverseGeocodeResult) {
    if (place.status.code == 200) {
        const {_category, _type} = place.results[0].components
        return _category === "transportation" && _type === "aeroway"
    }
    return undefined
}

function _hospital(place: ReverseGeocodeResult) {
    if (place.status.code == 200) {
        const {_category, _type} = place.results[0].components
        return _category === "health" && _type === "hospital"
    }
    return undefined
}
