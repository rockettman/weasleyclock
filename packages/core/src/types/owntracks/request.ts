import { z } from "zod";

const OwntracksTour = z.object({
    label: z.string(),
    from: z.string(),
    to: z.string(),
    uuid: z.string(),
    url: z.string(),
})

export const OwntracksTourRequest = z.object({
    _type: z.literal("request"),
    request: z.literal("tour"),
    tour: z.object({
        label: z.string(),
        from: z.string(),
        to: z.string(),
    }),
})
export type OwntracksTourRequest = z.infer<typeof OwntracksTourRequest>

export const OwntracksTourResponse = z.object({
    _type: z.literal("cmd"),
    action: z.literal("response"),
    request: z.literal("tour"),
    status: z.number().int().positive(),
    tour: OwntracksTour,
})
export type OwntracksTourResponse = z.infer<typeof OwntracksTourResponse>

export const OwntracksToursRequest = z.object({
    _type: z.literal("request"),
    request: z.literal("tours"),
})
export type OwntracksToursRequest = z.infer<typeof OwntracksToursRequest>

export const OwntracksToursResponse = z.object({
    _type: z.literal("cmd"),
    action: z.literal("response"),
    request: z.literal("tours"),
    ntours: z.number().int().nonnegative(),
    tours: z.array(OwntracksTour),
})
export type OwntracksToursResponse = z.infer<typeof OwntracksToursResponse>

export const OwntracksUntourRequest = z.object({
    _type: z.literal("request"),
    request: z.literal("untour"),
    uuid: z.string(),
})
export type OwntracksUntourRequest = z.infer<typeof OwntracksUntourRequest>
