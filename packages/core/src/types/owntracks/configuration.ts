import { z } from "zod";
import { OwntracksWaypoint } from "./waypoint";

export const OwntracksConfiguration = z.object({
    // Shared
    _type: z.literal("configuration"),
    cleanSession: z.boolean(),
    clientId: z.string().optional(),
    cmd: z.boolean(),
    deviceId: z.string(),
    host: z.string().optional(),
    ignoreInaccurateLocations: z.number().int().nonnegative(),
    ignoreStaleLocations: z.number().int().nonnegative(),
    keepalive: z.number().int().nonnegative(),
    locatorDisplacement: z.number().int(),
    locatorInterval: z.number().int(),
    mode: z.number().int().refine(v => v == 0 || v == 3, "Must be 0 or 3"),
    monitoring: z.number().int().min(-1).max(2),
    mqttProtocolLevel: z.number().int().min(3).max(5),
    password: z.string(),
    port: z.number().int().nonnegative().optional(),
    pubQos: z.number().int().min(0).max(2),
    pubRetain: z.boolean(),
    pubTopicBase: z.string(),
    sub: z.boolean(),
    subQos: z.number().int().min(0).max(2),
    subTopic: z.string(),
    tid: z.string(),
    tls: z.boolean(),
    username: z.string(),
    waypoints: z.array(OwntracksWaypoint).optional(),
    ws: z.boolean(),

    // Android
    autostartOnBoot: z.boolean().optional(),
    locatorPriority: z.number().int().min(0).max(3).optional(),
    notificationLocation: z.boolean().optional(),
    opencageApiKey: z.string().optional(),
    ping: z.number().int().optional(),
    pubExtendedData: z.boolean(),
    remoteConfiguration: z.boolean().optional(),
    tlsClientCrtPassword: z.string().optional(),

    // iOS
    allowRemoteLocation: z.boolean().optional(),
    allowinvalidcerts: z.boolean().optional(),
    clientpkcs: z.string().optional(),
    downgrade: z.number().int().min(0).max(100).optional(),
    extendedData: z.boolean(),
    locked: z.boolean().optional(),
    maxHistory: z.number().int().nonnegative().optional(),
    osmTemplate: z.string().optional(),
    osmCopyright: z.string().optional(),
    passphrase: z.string().optional(),
    positions: z.number().int().nonnegative().optional(),
    ranging: z.boolean().optional(),

    // Unknown
    auth: z.boolean().optional(),
    encryptionKey: z.string().optional(),
    httpHeaders: z.string().optional(),
    url: z.string().optional(),
})
export type OwntracksConfiguration = z.infer<typeof OwntracksConfiguration>
