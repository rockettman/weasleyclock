import { z } from "zod";
import { OwntracksConfiguration } from "./configuration";
import { OwntracksTourRequest, OwntracksToursRequest, OwntracksUntourRequest } from "./request";
import { OwntracksLocation } from "./location";
import { OwntracksCmd } from "./cmd";
import { OwntracksBeacon, OwntracksTransition, OwntracksWaypoint, OwntracksWaypoints } from "./waypoint";

export * from "./location"
export * from "./waypoint"
export * from "./configuration"
export * from "./cmd"
export * from "./request"

export const OwntracksCard = z.object({
    _type: z.literal("card"),
    name: z.string().optional(),
    face: z.string().optional(),
})
export type OwntracksCard = z.infer<typeof OwntracksCard>

export const OwntracksEncrypted = z.object({
    _type: z.literal("encrypted"),
    data: z.string(),
})
export type OwntracksEncrypted = z.infer<typeof OwntracksEncrypted>

export const OwntracksLwt = z.object({
    _type: z.literal("lwt"),
    tst: z.number().int(),
})
export type OwntracksLwt = z.infer<typeof OwntracksLwt>

export const OwntracksSteps = z.object({
    _type: z.literal("steps"),
    tst: z.number().int(),
    steps: z.number().int(),
    from: z.number().int(),
    to: z.number().int(),
})
export type OwntracksSteps = z.infer<typeof OwntracksSteps>

export const OwntracksMessage = z.union([
    OwntracksBeacon,
    OwntracksCard,
    OwntracksCmd,
    OwntracksConfiguration,
    OwntracksEncrypted,
    OwntracksLocation,
    OwntracksLwt,
    OwntracksTourRequest,
    OwntracksToursRequest,
    OwntracksUntourRequest,
    OwntracksSteps,
    OwntracksTransition,
    OwntracksWaypoint,
    OwntracksWaypoints,
])
export type OwntracksMessage = z.infer<typeof OwntracksMessage>
