import { z } from "zod";

export const OwntracksLocation = z.object({
    _type: z.literal("location"),
    acc: z.number().int().nonnegative().optional(),
    alt: z.number().int().optional(),
    batt: z.number().int().min(0).max(100).optional(),
    bs: z.number().int().min(0).max(3),
    cog: z.number().int().optional(),
    lat: z.number(),
    lon: z.number(),
    rad: z.number().int().optional(),
    t: z.enum(["p", "c", "b", "r", "u", "t", "v"]).optional(),
    tid: z.string().optional(),
    tst: z.number().int(),
    vac: z.number().int().nonnegative().optional(),
    vel: z.number().int().nonnegative().optional(),
    p: z.number().optional(),
    poi: z.string().optional(),
    conn: z.enum(["w", "o", "m"]).optional(),
    tag: z.string().optional(),
    inregions: z.string().array().optional(),
    inrids: z.string().array().optional(),
    SSID: z.string().optional(),
    BSSID: z.string().optional(),
    created_at: z.number().int().optional(),
    m: z.number().int().min(-1).max(2).optional(),
})
export type OwntracksLocation = z.infer<typeof OwntracksLocation>
