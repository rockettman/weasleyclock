import { z } from "zod";

export const OwntracksWaypoint = z.object({
    _type: z.literal("waypoint"),
    desc: z.string(),
    lat: z.number().optional(),
    lon: z.number().optional(),
    rad: z.number().int().optional(),
    tst: z.number().int(),
    uuid: z.string().optional(),
    major: z.number().int().nonnegative().optional(),
    minor: z.number().int().nonnegative().optional(),
    rid: z.string(),
})
export type OwntracksWaypoint = z.infer<typeof OwntracksWaypoint>

export const OwntracksWaypoints = z.object({
    _type: z.literal("waypoints"),
    _creator: z.string().optional(),
    waypoints: z.array(OwntracksWaypoint),
})
export type OwntracksWaypoints = z.infer<typeof OwntracksWaypoints>

export const OwntracksBeacon = z.object({
    _type: z.literal("beacon"),
    desc: z.string(),
    uuid: z.string(),
    major: z.number().int().nonnegative(),
    minor: z.number().int().nonnegative(),
    tst: z.number().int(),
    acc: z.number().int().nonnegative(),
    rssi: z.number().int().nonnegative(),
    prox: z.number().int().min(0).max(3),
})
export type OwntracksBeacon = z.infer<typeof OwntracksBeacon>

export const OwntracksTransition = z.object({
    _type: z.literal("transition"),
    wtst: z.number().int(),
    lat: z.number().optional(),
    lon: z.number().optional(),
    tst: z.number().int(),
    acc: z.number().int().nonnegative(),
    tid: z.string().optional(),
    event: z.enum(["enter", "leave"]),
    desc: z.string().optional(),
    t: z.enum(["c", "b", "l"]).optional(),
    rid: z.string(),
})
export type OwntracksTransition = z.infer<typeof OwntracksTransition>
