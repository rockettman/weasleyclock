
export enum LocationType {
    Home       = "HOME",
    Out        = "OUT",
    Abroad     = "ABROAD",
    InTransit  = "IN_TRANSIT",
    Lost       = "LOST",

    Friends    = "FRIENDS",
    Family     = "FAMILY",
    Work       = "WORK",
    HappyPlace = "HAPPY_PLACE",

    Airport    = "AIRPORT",
    Hospital   = "HOSPITAL",
}
