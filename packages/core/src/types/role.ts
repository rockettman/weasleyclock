
export enum Role {
    Owner  = "OWNER",
    Admin  = "ADMIN",
    Member = "MEMBER",
}
