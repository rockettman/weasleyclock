import { IoTCustomAuthorizerHandler } from "aws-lambda";
import { Resource } from "sst";

export const handler: IoTCustomAuthorizerHandler = async (evt, ctx) => {
  console.log(`AUTH EVENT: ${JSON.stringify(evt)}`)
  const [, , , region, accountId] = ctx.invokedFunctionArn.split(":")

  const uname = evt.protocolData.mqtt?.username ?? ""
  const passwd = Buffer.from(
    evt.protocolData.mqtt?.password ?? "",
    "base64"
  ).toString()
  console.log(`USERNAME: ${uname}`)
  console.log(`PASSWORD: ${passwd}`)

  const isAuthenticated = passwd === Resource.MqttPass.value
  const principalId = evt.protocolData.mqtt?.clientId ?? Date.now().toString()
  const subTopics: string[] = [`owntracks/${uname}/*/cmd`]
  const pubTopics: string[] = [`owntracks/${uname}/*`]

  return {
    isAuthenticated,
    principalId,
    disconnectAfterInSeconds: 60 * 60 * 24,
    refreshAfterInSeconds: 60 * 5,
    policyDocuments: [
      {
        Version: "2012-10-17",
        Statement: [
          {
            Action: "iot:Connect",
            Effect: "Allow",
            Resource: "*",
          },
          {
            Action: "iot:Receive",
            Effect: "Allow",
            Resource: subTopics.map(
              (t) => `arn:aws:iot:${region}:${accountId}:topic/${t}`
            ),
          },
          {
            Action: "iot:Subscribe",
            Effect: "Allow",
            Resource: subTopics.map(
              (t) => `arn:aws:iot:${region}:${accountId}:topicfilter/${t}`
            ),
          },
          {
            Action: "iot:Publish",
            Effect: "Allow",
            Resource: pubTopics.map(
              (t) => `arn:aws:iot:${region}:${accountId}:topic/${t}`
            ),
          },
        ]
      },
    ]
  }
}
