import { MetricsLogger, MetricUnit } from "@weasleyclock/core/metrics";
import { OwntracksMessage } from "@weasleyclock/core/types";
import { IoTHandler } from "aws-lambda";
import { z } from "zod";

const metrics = MetricsLogger("Owntracks")

export const owntracks: IoTHandler = async (evt) => {
  metrics.addMetric("Messages", MetricUnit.Count, 1)

  const msgSchema = z.object({
    topic: z.string(),
    body: OwntracksMessage,
  })

  try {
    const msg = msgSchema.parse(evt)
    metrics.addMetric(msg.body._type, MetricUnit.Count, 1)
    metrics.addMetric("InvalidMessages", MetricUnit.Count, 0)
  } catch (e) {
    console.log(`INVALID MSG: ${JSON.stringify(evt, null, 2)}`)
    console.log(e)
    metrics.addMetric("InvalidMessages", MetricUnit.Count, 1)
  }

  metrics.publishStoredMetrics()
}
