import { Handler } from "aws-lambda";
import { Resource } from "sst";

export const accessDb: Handler = async (_) => {
  return {
    statusCode: 200,
    body: `Able to access ${Resource.Database.name}`,
  };
};

export const accessIotCore: Handler = async (_) => {
  return {
    statusCode: 200,
    body: `Able to access ${Resource.IoTCore.endpoint}`,
  };
};

export const reverseGeocode: Handler = async (_) => {
  return {
    statusCode: 200,
    body: `Able to reverse geocode`,
  }
}
