import { OwntracksLocation, OwntracksLwt } from "@weasleyclock/core/types";
import { IoTHandler } from "aws-lambda";
import { z } from "zod";
import { ingestUserLocation } from "@weasleyclock/core/ingest-location";

export const handler: IoTHandler = async (evt) => {

  const msgSchema = z.object({
    topic: z.string(),
    body: z.union([OwntracksLocation, OwntracksLwt]),
  })
  const msg = msgSchema.parse(evt)

  if (msg.body._type === "lwt") {
    console.info("Ignoring LWT message")
    return
  }

  const user = msg.topic.split("/")[1]
  const locUpdate = msg.body

  await ingestUserLocation({ user, locUpdate })
}
