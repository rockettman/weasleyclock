import { User } from "@weasleyclock/core/model";
import { PreSignUpTriggerHandler } from "aws-lambda";

export const handler: PreSignUpTriggerHandler = async (evt) => {
  await User.create(evt.userName)
  return evt
}
