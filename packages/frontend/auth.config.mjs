import Cognito from '@auth/core/providers/cognito';
import { defineConfig } from 'auth-astro';

export default defineConfig({
  secret: import.meta.env.AUTH_SECRET,
  providers: [
    Cognito({
      clientId: import.meta.env.COGNITO_CLIENT_ID,
      clientSecret: import.meta.env.COGNITO_CLIENT_SECRET,
      issuer: import.meta.env.COGNITO_ISSUER,
    }),
  ],
});
