import { defineConfig } from 'astro/config';
import aws from "astro-sst";
import tailwind from "@astrojs/tailwind";

import auth from "auth-astro";

// https://astro.build/config
export default defineConfig({
  output: "server",
  adapter: aws(),
  integrations: [tailwind(), auth()]
});