
export const database = new sst.aws.Dynamo("Database", {
  fields: {
    pk: "string",
    sk: "string",
    gsi1pk: "string",
    gsi1sk: "string",
    gsi2pk: "string",
    gsi2sk: "string",
  },
  primaryIndex: {
    hashKey: "pk",
    rangeKey: "sk",
  },
  globalIndexes: {
    gsi1: {
      hashKey: "gsi1pk",
      rangeKey: "gsi1sk",
    },
    gsi2: {
      hashKey: "gsi2pk",
      rangeKey: "gsi2sk",
    },
  },
  transform: {
    table: {
      billingMode: "PAY_PER_REQUEST",
      serverSideEncryption: { enabled: true },
      pointInTimeRecovery: { enabled: $app.stage === "prod" },
    },
  },
});

new sst.aws.Function("TestDbAccess", {
  handler: "packages/functions/src/tests.accessDb",
  link: [database],
});
