import { database } from "./database"
import { isDev } from "./util"

export module Auth {

  export const userPool = new sst.aws.CognitoUserPool("UserPool", {
    usernames: ["email"],
    triggers: {
      preSignUp: {
        handler: "packages/functions/src/register-user.handler",
        link: [database],
        live: false,
      },
    },
  })

  new aws.cognito.UserPoolDomain("UserPoolDomain", {
    userPoolId: userPool.nodes.userPool.id,
    domain: `${$app.name}-${$app.stage}`,
  })

  export const issuer = $interpolate`https://cognito-idp.us-east-2.amazonaws.com/${userPool.id}`

  const callbackUrls = [
    "https://poppyclock.com/api/auth/callback/cognito",
    ... isDev() ? ["http://localhost:4321/api/auth/callback/cognito"] : [],
  ]

  export const appClient = userPool.addClient("AppClient", {
    transform: {
      client: {
        generateSecret: true,
        callbackUrls,
      }
    }
  })

}
