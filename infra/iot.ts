import { database } from "./database";
import { opencageApiKey } from "./geocode";

const mqttPass = new sst.Secret("MqttPass")

export const iotcore = new sst.aws.Realtime("IoTCore", {
  authorizer: {
    handler: "packages/functions/src/iot-authorizer.handler",
    link: [mqttPass],
    live: false,
  },
});

new sst.aws.Function("TestIotAccess", {
  handler: "packages/functions/src/tests.accessIotCore",
  link: [iotcore],
});

iotcore.subscribe(
  {
    handler: "packages/functions/src/publish-metric.owntracks",
    live: false,
  },
  {
    filter: "owntracks/#",
    transform: {
      topicRule: {
        sql: "SELECT topic() as topic, * as body FROM 'owntracks/#'",
      }
    }
  }
)

export module OwntracksMetrics {

  export const Messages = new awsx.classic.cloudwatch.Metric({
    namespace: "WeasleyClock",
    dimensions: {
      stage: $app.stage,
      service: "Owntracks",
    },
    name: "Messages",
    unit: "Count",
    statistic: "Sum",
  })

  export const InvalidMessages = new awsx.classic.cloudwatch.Metric({
    namespace: "WeasleyClock",
    dimensions: {
      stage: $app.stage,
      service: "Owntracks",
    },
    name: "InvalidMessages",
    unit: "Count",
    statistic: "Sum",
  })

  export const MessageType = new awsx.classic.cloudwatch.ExpressionWidgetMetric(
    `SEARCH('{WeasleyClock,stage,service} stage="${$app.stage}" service="Owntracks" NOT "Messages" NOT "InvalidMessages"','Sum')`
  )

}

const ingestLocation = iotcore.subscribe(
  {
    handler: "packages/functions/src/ingest-location.handler",
    link: [database, opencageApiKey],
    live: false,
  },
  {
    filter: "owntracks/+/+",
    transform: {
      topicRule: {
        sql: "SELECT topic() as topic, * as body FROM 'owntracks/+/+'",
      }
    }
  }
)

export module LocationProcessingMetrics {

  export const Processed = new awsx.classic.cloudwatch.Metric({
    namespace: "WeasleyClock",
    dimensions: {
      stage: $app.stage,
      service: "Core",
    },
    name: "LocationsProcessed",
    unit: "Count",
    statistic: "Sum",
  })

  export const Latency = new awsx.classic.cloudwatch.Metric({
    namespace: "WeasleyClock",
    dimensions: {
      stage: $app.stage,
      service: "Core",
    },
    name: "LocationProcessingLatency",
  })

  export const Errors = new awsx.classic.cloudwatch.Metric({
    namespace: "AWS/Lambda",
    dimensions: {
      FunctionName: $interpolate`${ingestLocation.nodes.function.name}`,
    },
    name: "Errors",
    unit: "Count",
    statistic: "Sum",
  })

}
