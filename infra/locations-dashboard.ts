import { LocationProcessingMetrics } from "./iot";

export const locationsDash = new awsx.classic.cloudwatch.Dashboard("LocationsDashboard", {
  name: `${$app.name}-${$app.stage}-LocationsDashboard`,
  widgets: [

    new awsx.classic.cloudwatch.TextWidget({
      markdown: `
### Location Processing
Subscribe to location updates sent by Owntracks to IoT Core and determine clock status
for the user from their lat/lon.
`,
    }),

    new awsx.classic.cloudwatch.LineGraphMetricWidget({
      title: "LocationProcessingErrors",
      yAxis: { right: { min: 0 } },
      metrics: LocationProcessingMetrics.Errors.withYAxis("right"),
    }),

    new awsx.classic.cloudwatch.LineGraphMetricWidget({
      yAxis: { right: { min: 0 } },
      metrics: [
        LocationProcessingMetrics.Latency.with({
          statistic: "Average",
          label: "avg",
          yAxis: "right",
        }),
        LocationProcessingMetrics.Latency.with({
          extendedStatistic: 90,
          label: "p90",
          yAxis: "right",
        }),
        LocationProcessingMetrics.Latency.with({
          extendedStatistic: 99,
          label: "p99",
          yAxis: "right",
        }),
      ],
    }),

    new awsx.classic.cloudwatch.LineGraphMetricWidget({
      yAxis: { right: { min: 0 } },
      metrics: LocationProcessingMetrics.Processed.withYAxis("right"),
    }),

  ],
})
