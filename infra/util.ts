
export function isDev(): boolean {
  return $app.stage !== "prod"
}

export function isProd(): boolean {
  return $app.stage === "prod"
}
