import { web } from "./frontend";
import { opencageDash } from "./geocode";
import { LocationProcessingMetrics } from "./iot";
import { locationsDash } from "./locations-dashboard";
import { owntracksDash } from "./owntracks-dashboard";


export const dashboard = new awsx.classic.cloudwatch.Dashboard("Dashboard", {
  name: `${$app.name}-${$app.stage}-Dashboard`,
  widgets: [

    new awsx.classic.cloudwatch.TextWidget({
      height: 12,
      markdown: $interpolate`

## WeasleyClock

Locations published from a user's [Owntracks](https://owntracks.org/)
app are async processed to assign a location status for that user in
each clock they belong to.

Users view clocks and configure location boundaries through the web app.

### Resources
- Architecture Diagram (TODO)
- [Code Repo](https://gitlab.com/rockettman/weasleyclock)

#### Component Dashboards
- Web App (TODO)
- [Location Processing](${locationsDash.url})

#### Infrastructure Dashboards
- [CloudFront Distro](https://us-east-1.console.aws.amazon.com/cloudfront/v4/home#/monitoring/distribution/${web.nodes.cdn.nodes.distribution.id})
- [IoT Core](https://console.aws.amazon.com/iot/home#/dashboard)
- [IoT Rules](https://console.aws.amazon.com/cloudwatch/home#home:dashboards/IoT?~(selectedResourceGroup~'${$app.name}_${$app.stage}))
- [Database](https://console.aws.amazon.com/cloudwatch/home#home:dashboards/DynamoDB?~(selectedResourceGroup~'${$app.name}_${$app.stage}))

#### Dependency Dashboards
- [OpenCage](${opencageDash.url}) - geocoding
- [Owntracks](${owntracksDash.url}) - location updates
`,
    }),

    new awsx.classic.cloudwatch.SingleNumberMetricWidget({
      metrics: LocationProcessingMetrics.Processed,
    }),

  ],
})
