import { OwntracksMetrics } from "./iot";

export const owntracksDash = new awsx.classic.cloudwatch.Dashboard("OwntracksDashboard", {
  name: `${$app.name}-${$app.stage}-OwntracksDashboard`,
  widgets: [

    new awsx.classic.cloudwatch.TextWidget({
      markdown: `
### Owntracks
Open source location tracking mobile app for iOS and Android which publishes user lat/lon location
updates to the IoT Core MQTT broker.

**See Owntracks [website](https://owntracks.org/) and [docs](https://owntracks.org/booklet/)**
`,
    }),

    new awsx.classic.cloudwatch.LineGraphMetricWidget({
      width: 18,
      title: "Messages",
      yAxis: {
        left: { min: 0 },
        right: { min: 0 },
      },
      metrics: [
        OwntracksMetrics.Messages.withYAxis("right"),
        OwntracksMetrics.InvalidMessages.withYAxis("left"),
      ],
    }),

    new awsx.classic.cloudwatch.TextWidget({
      markdown: `
### Key Message Types
- location - user lat/lon message
- lwt - published when MQTT broker loses contact with app

**See [Owntracks types](https://owntracks.org/booklet/tech/json/) for more details**
`,
    }),

    new awsx.classic.cloudwatch.LineGraphMetricWidget({
      width: 18,
      title: "Message Types",
      yAxis: { left: { min: 0 } },
      metrics: OwntracksMetrics.MessageType,
    }),

  ],
})
