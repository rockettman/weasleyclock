
export const opencageApiKey = new sst.Secret("OpenCageApiKey")

new sst.aws.Function("TestReverseGeocode", {
  handler: "packages/functions/src/tests.reverseGeocode",
  link: [opencageApiKey],
})


module OpenCageMetrics {

  export const Requests = new awsx.classic.cloudwatch.Metric({
    namespace: "WeasleyClock",
    dimensions: {
      stage: $app.stage,
      service: "OpenCage",
    },
    name: "Requests",
    statistic: "Sum",
  })

  export const Latency = new awsx.classic.cloudwatch.Metric({
    namespace: "WeasleyClock",
    dimensions: {
      stage: $app.stage,
      service: "OpenCage",
    },
    name: "Latency",
  })

  export const ResponseCodes = new awsx.classic.cloudwatch.ExpressionWidgetMetric(
    `SEARCH('{WeasleyClock,stage,service} stage="${$app.stage}" service="OpenCage" NOT "Requests" NOT "Latency" NOT "RemainingQuota"','Sum')`
  )

  export const RemainingQuota = new awsx.classic.cloudwatch.Metric({
    namespace: "WeasleyClock",
    dimensions: {
      stage: $app.stage,
      service: "OpenCage",
    },
    name: "RemainingQuota",
  })
}

export const opencageDash = new awsx.classic.cloudwatch.Dashboard("OpenCageDashboard", {
  name: `${$app.name}-${$app.stage}-OpenCageDashboard`,
  widgets: [

    new awsx.classic.cloudwatch.TextWidget({
      markdown: `
### OpenCage
Geocoding provider used by the WeasleyClock to reverse-geocode user locations.

**See OpenCage [website](https://opencagedata.com) and [api docs](https://opencagedata.com/api)**

The free trial includes
- 2,500 requests/day hard limit that resets at 24:00 UTC
- 1 request/sec

**See [OpenCage pricing](https://opencagedata.com/pricing) for more details**
`,
    }),

    new awsx.classic.cloudwatch.SingleNumberMetricWidget({
      metrics: OpenCageMetrics.RemainingQuota.withStatistic("Minimum").withPeriod(300),
    }),

    new awsx.classic.cloudwatch.LineGraphMetricWidget({
      width: 12,
      yAxis: {
        left: { min: 0, max: 3000 },
        right: { min: 0, max: 3000 },
      },
      metrics: [
        OpenCageMetrics.RemainingQuota.withStatistic("Minimum").withYAxis("right"),
      ],
    }),

    new awsx.classic.cloudwatch.TextWidget({
      markdown: `
### Key Response Codes
- 200 - OK
- 400 - Invalid request
- 401 - Unable to authenticate
- 402 - Quota exceeded
- 403 - Forbidden/disabled
- 408 - Timeout
- 410 - Request too long
- 429 - Too many requests
- 503 - Internal server error

**See [OpenCage Response Codes](https://opencagedata.com/api#codes)**
`,
    }),

    new awsx.classic.cloudwatch.LineGraphMetricWidget({
      title: "Responses",
      yAxis: { left: { min: 0 } },
      metrics: OpenCageMetrics.ResponseCodes,
    }),

    new awsx.classic.cloudwatch.LineGraphMetricWidget({
      yAxis: { right: { min: 0 } },
      metrics: [
        OpenCageMetrics.Latency.with({
          statistic: "Average",
          label: "avg",
          yAxis: "right",
        }),
        OpenCageMetrics.Latency.with({
          extendedStatistic: 90,
          label: "p90",
          yAxis: "right",
        }),
        OpenCageMetrics.Latency.with({
          extendedStatistic: 99,
          label: "p99",
          yAxis: "right",
        }),
      ],
    }),

    new awsx.classic.cloudwatch.LineGraphMetricWidget({
      yAxis: { right: { min: 0 } },
      metrics: OpenCageMetrics.Requests.withYAxis("right"),
    }),

  ],
})
