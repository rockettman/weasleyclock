import { Auth } from "./auth";
import { iotcore } from "./iot";

export const authSecret = new sst.Secret("AuthSecret")

export const web = new sst.aws.Astro("Frontend", {
  path: "packages/frontend",
  environment: {
    PUBLIC_IOT_ENDPOINT: iotcore.endpoint,
    AUTH_SECRET: authSecret.value,
    COGNITO_CLIENT_ID: Auth.appClient.id,
    COGNITO_CLIENT_SECRET: Auth.appClient.nodes.client.clientSecret,
    COGNITO_ISSUER: Auth.issuer,
  },
})
