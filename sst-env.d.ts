/* tslint:disable */
/* eslint-disable */
import "sst"
declare module "sst" {
  export interface Resource {
    AppClient: {
      id: string
      secret: string
      type: "sst.aws.CognitoUserPoolClient"
    }
    AuthSecret: {
      type: "sst.sst.Secret"
      value: string
    }
    Database: {
      name: string
      type: "sst.aws.Dynamo"
    }
    Frontend: {
      type: "sst.aws.Astro"
      url: string
    }
    IoTCore: {
      authorizer: string
      endpoint: string
      type: "sst.aws.Realtime"
    }
    MqttPass: {
      type: "sst.sst.Secret"
      value: string
    }
    OpenCageApiKey: {
      type: "sst.sst.Secret"
      value: string
    }
    TestDbAccess: {
      name: string
      type: "sst.aws.Function"
    }
    TestIotAccess: {
      name: string
      type: "sst.aws.Function"
    }
    TestReverseGeocode: {
      name: string
      type: "sst.aws.Function"
    }
    UserPool: {
      id: string
      type: "sst.aws.CognitoUserPool"
    }
  }
}
export {}